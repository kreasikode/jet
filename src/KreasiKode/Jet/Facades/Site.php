<?php namespace KreasiKode\Jet\Facades;

use Illuminate\Support\Facades\Facade;

/**
* Site
*/
class Site extends Facade
{

    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor() { return 'jet.site'; }
}