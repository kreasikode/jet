<?php namespace KreasiKode\Jet;

use Illuminate\Support\ServiceProvider;
use Illuminate\Foundation\AliasLoader;
use KreasiKode\Jet\Site;

class JetServiceProvider extends ServiceProvider {

	/**
	 * Indicates if loading of the provider is deferred.
	 *
	 * @var bool
	 */
	protected $defer = false;

	/**
	 * Bootstrap the application events.
	 *
	 * @return void
	 */
	public function boot()
	{
		$this->package('kreasi-kode/jet');

		// $this->app->booting(function()
		// {
			// $loader = AliasLoader::getInstance();
			// $loader->alias('Asset', 'Orchestra\Support\Facades\Asset');
			// $loader->alias('Site', 'KreasiKode\Jet\Facades\Site');
		// });

		include __DIR__."/../../start.php";
	}

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		// register Orchestra/Asset
		$this->app->register('Orchestra\Asset\AssetServiceProvider');
		// register TwigBridge
		$this->app->register('TwigBridge\TwigServiceProvider');

		// register Site
		$this->app['jet.site'] = $this->app->share(function ($app)
		{
			return new Site($app);
		});
	}

	/**
	 * Get the services provided by the provider.
	 *
	 * @return array
	 */
	public function provides()
	{
		return array('jet.site');
	}

}