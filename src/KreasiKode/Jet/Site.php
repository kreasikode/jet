<?php
namespace KreasiKode\Jet;

/**
* Site
*/
class Site {

    /**
     * Application instance.
     *
     * @var \Illuminate\Foundation\Application
     */
    protected $app = null;

    /**
     * Data for site.
     *
     * @var array
     */
    protected $items = array();

    protected $theme;

    /**
     * Construct a new instance.
     *
     * @param  \Illuminate\Foundation\Application   $app
     * @return void
     */
    public function __construct($app)
    {
        $this->app = $app;
        $this->theme = new Theme($app);
    }

    public function theme()
    {
        return $this->theme;
    }

    /**
     * Get a site value.
     *
     * @param  string   $key
     * @param  mixed    $default
     * @return mixed
     */
    public function get($key, $default = null)
    {
        return array_get($this->items, $key, $default);
    }

    /**
     * Set a site value.
     *
     * @param  string   $key
     * @param  mixed    $value
     * @return mixed
     */
    public function set($key, $value = null)
    {
        return array_set($this->items, $key, $value);
    }

    /**
     * Push a site array value.
     *
     * @param  string   $key
     * @param  mixed    $value
     * @return mixed
     */
    public function push($key, $value = null)
    {
        $exists = array_get($this->items, $key, array());
        array_push($exists, $value);
        return array_set($this->items, $key, $exists);
    }

    /**
     * Check if site key has a value.
     *
     * @param  string   $key
     * @return boolean
     */
    public function has($key)
    {
        return ! is_null($this->get($key));
    }

    /**
     * Remove a site key.
     *
     * @param  string   $key
     * @return void
     */
    public function forget($key)
    {
        return array_forget($this->items, $key);
    }

    /**
     * Get all available items.
     *
     * @return array
     */
    public function all()
    {
        return $this->items;
    }
}