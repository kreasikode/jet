<?php
namespace KreasiKode\Jet;

use Asset, Config, View;

/**
* Theme
*/
class Theme {

    protected $theme;
    protected $settings;

    public function __construct()
    {
        $this->theme = Config::get('jet::theme.default');
        $settings = Config::get('jet::theme.settings');
        $this->settings = $settings[$this->theme];
    }

    public function init()
    {
        View::share('theme', $this->settings['path'].'/');
        foreach ($this->settings['assets'] as $value) {
            Asset::add($value[0], $value[1], isset($value[2]) ? $value[2] : null);
        }
    }
}