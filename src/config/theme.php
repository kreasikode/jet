<?php

return array(
    'default' => 'aql',

    'settings' => array(
        'aql' => array(
            'path' => 'jet::theme-aql',
            'assets' => array(
                array('aql-css', 'packages/kreasi-kode/jet/theme-aql/css/aql-style.css', 'bst-css'),
                array('aql-js', 'packages/kreasi-kode/jet/theme-aql/js/aql.js', 'jquery')
            )
        )
    )
);