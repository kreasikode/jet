<?php

Event::listen('twigbridge.twig', function($twig) {

    Asset::add('bst-css', 'packages/kreasi-kode/jet/vendor/bootstrap/css/bootstrap.css');
    Asset::add('bst-responsive-css', 'packages/kreasi-kode/jet/vendor/bootstrap/css/bootstrap-responsive.css', 'bst-css');

    Asset::add('jquery', 'packages/kreasi-kode/jet/vendor/bootstrap/js/jquery.js');
    Asset::add('bst-js', 'packages/kreasi-kode/jet/vendor/bootstrap/js/bootstrap.min.js', 'jquery');

    Site::theme()->init();

});