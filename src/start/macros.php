<?php

Form::macro('datepicker', function($name, $value = null, $options = array(), $scripts = null) {

    Asset::container('additional')->add(
        'datepicker-css',
        'packages/kreasi-kode/jet/vendor/datepicker/css/datepicker.css'
    );
    Asset::container('additional')->add(
        'datepicker-js',
        'packages/kreasi-kode/jet/vendor/datepicker/js/bootstrap-datepicker.js',
        'jquery'
    );

    if (false !== $scripts) {
        if (is_null($scripts)) {
            $scripts = "$('input[name={$name}]').datepicker()";
        }
        Site::push('jqready', $scripts);
    }

    $options['class'] = isset($options['class'])
        ? $options['class'] . ' datepicker' : 'datepicker';

    return Form::text($name, $value, $options);
});

Form::macro('select2', function($name, $list = array(), $selected = null, $options = array(), $scripts = null) {

    Asset::container('additional')->add(
        'select2-css',
        'packages/kreasi-kode/jet/vendor/select2-3.4.1/select2.css'
    );
    Asset::container('additional')->add(
        'select2-bst-css',
        'packages/kreasi-kode/jet/vendor/select2-3.4.1/select2-bootstrap.css',
        'bst'
    );
    Asset::container('additional')->add(
        'select2-js',
        'packages/kreasi-kode/jet/vendor/select2-3.4.1/select2.min.js',
        'jquery'
    );

    if (false !== $scripts) {
        if (is_null($scripts)) {
            $scripts = "$('select[name={$name}]').select2()";
        }
        Site::push('jqready', $scripts);
    }

    return Form::select($name, $list, $selected, $options);
});